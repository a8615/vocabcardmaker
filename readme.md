# Vocabulary card maker

Vocabulary card maker is a python program for making vocabulary cards automatically, it was specifically designed for an APHuG class.

## Getting Started

### Prerequisites

1. [python](http://python.org)
2. vocabcardmaker.py

### Installing

1. Have python installed on your device, download vocabcardmaker.py, and open a terminal/command prompt.
2. Run: pip install reportlab bs4
3. Run: python vocabcardmaker.py
4. Enter the information prompted and you will get a pdf file.

### Support

If you give me your email in person, I will give you an offer of sending you executable files for your convenience, and giving you support if you have any problems related to this program.

## Usage

Although this program does the easy part of making your vocab cards, it cannot add the example sentences that you need, so add that manually. To add custom fonts, provide the .ttf file's name as an extra argument when you run the python program and use that filename as a font in the program, I may explain in greater detail in the future.

## Built With

* [reportlab](https://reportlab.com) - Used for creating pdfs
* [beautifulsoup](https://www.crummy.com/software/BeautifulSoup/) - Used in the process of getting images

## Authors

* **Geographicallylazy** (https://gitlab.com/geographicallylazy)

## License

For now, just give me credit and share your changes with others, please.