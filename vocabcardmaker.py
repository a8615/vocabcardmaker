###Created by geographicallylazy on Feb 5 2022, please credit me and share your changes w/ others.
from reportlab.pdfgen.canvas import Canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import random
import sys
from bs4 import BeautifulSoup
import requests
from reportlab.lib.utils import ImageReader

def randgoogleimg(inp):
    url = f'https://www.google.com/search?q={inp}&tbm=isch'
    soup = BeautifulSoup(requests.get(url).text, 'html.parser')
    image = ImageReader((soup.findAll('img')[random.randint(1, 2)])['src'])
    return image


#takes argument given from command line and makes it available as a font
if len(sys.argv) > 1:
    pdfmetrics.registerFont(TTFont(sys.argv[1], sys.argv[1]))


def draws(pixls, inp): canvas.drawString(pixls[0], pixls[1], inp)

vocablist = ""

defaultfonts = ['Helvetica', 'Times-Roman']

#these variables will be used if the user doesn't use the advanced settings
greatestlength = len('State that contains two or more ethnic groups with traditions of self')
resol = [random.randint(750,850), random.randint(550,650)]
dloca = [random.randint(25,75), random.randint(500,550)]
sloca = [random.randint(175,225),random.randint(75,125)]
wloca = [random.randint(175,225), random.randint(275,325)]
color = [1, 1, 1]
aloca = [random.randint(550,650), random.randint(275,325)]


#under here the program takes all its inputs
x = input('Enter your vocabulary list (make sure its seperated by -[or]– and "⏎"):')
vocablist += x

while x:
    x = input()
    vocablist += "\n" + x

print('\nTo get default settings, you can just press enter.')

font = input("Enter font (Ex. Times-Roman):")
if not font:
    font = defaultfonts[random.randint(0,1)]

fontsize = input("Enter font size:")
while not isinstance(fontsize, int):
    if not fontsize:
        fontsize = str(random.randint(20,22))
    try:
        fontsize = int(fontsize)
    except ValueError:
        print("Error: \nFont size needs to be a number, try again. \n")
        fontsize = input("Enter font size:")

outputfile = input("Enter name for output file (Ex. vocabcards):")
if not outputfile:
    outputfile = 'vocabcards'

autopic = input("Would you like auto-picture (experimental)? (y/n):")

if input("Would you like advanced settings? (y/n):") == "y":
    greatestlength = int(input("Enter greatest length for lines:"))
    resol = [int(input("Enter x of page:")),int(input("Enter y of page:"))]
    dloca = [int(input("Enter x of definition:")),int(input("Enter y of definition:"))]
    sloca = [int(input("Enter x of sentence:")),int(input("Enter y of sentence:"))]
    wloca = [int(input("Enter x of word:")),int(input("Enter y of word:"))]
    if autopic == "y":
        aloca = [int(input("Enter x of picture:")),int(input("Enter y of picture:"))]
    color = [int(input("Enter r of color:"))/255,int(input("Enter g of color:"))/255, int(input("Enter b of color:"))/255]

#here the vocablist is parsed
vocablist = vocablist.split('\n')
for i in range(0,len(vocablist)-1):
    vocablist[i] = vocablist[i].split(' – ') if '–' in vocablist[i] else vocablist[i].split(' - ')

#this part actually starts making the pdf
canvas = Canvas(outputfile + ".pdf", pagesize=(resol[0],resol[1]))

for i in range(0,len(vocablist)-1):
    try:
        canvas.setFont(font, fontsize)
    except KeyError as ex:
        print(f"Error: \n{ex} is not an available font, try again. \n")
        break
        
    canvas.setFillColorRGB(color[0],color[1],color[2])
    canvas.rect(0,0,resol[0],resol[1],stroke=0,fill=1)
    canvas.setFillColorRGB(0,0,0)

    if autopic == "y":
        canvas.drawImage(randgoogleimg(vocablist[i][0] + "+APHuG"),aloca[0],aloca[1],mask='auto')

    #the next part is what makes sure the definition on the slide fits on the page
    try:
        vocablist1 = vocablist[i][1].split()
    except IndexError:
        print('Error: \nSeperation of vocabulary list didn`t work, you should probably try again and make sure to have the list seperated with " - " or " – ", and "⏎" correctly. \n')

    def adddefline(grlen):
        vocablist2 = ""
        while len(vocablist2) < grlen and vocablist1 and (len(vocablist2 + vocablist1[0])) < grlen:
            vocablist2 += vocablist1[0] + " "
            del vocablist1[0]
        return vocablist2
    
    draws(dloca, "Definition: " + adddefline(greatestlength - 12))

    n = 1
    while vocablist1:
        canvas.drawString(dloca[0], dloca[1] - n * 25, adddefline(greatestlength))
        n += 1

    draws(wloca, "Word: " + vocablist[i][0])
    draws(sloca, "Sentence: ")

    canvas.showPage()

canvas.save()